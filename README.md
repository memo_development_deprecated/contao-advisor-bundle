# Memo Advisor Bundle

## About

With the Advisor Bundle you can add advisor search

## Installation

Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repos (not on packagist.org) to your composer.json:

```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-foundation-bundle"
  },
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-category-bundle"
  },
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-advisor-bundle"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/contao-advisor-bundle": "^0.0",
```

## Usage
tbd

## Features
* tbd

## Requirements

* PHP 7.1+
* Contao 4.4+ (tested on Contao 4.9, some features only work with Contao 4.9)
* Foundation-Bundle
* Category-Bundle

## Contributing / Reporting issues

Bug reports and pull requests are welcome - via Bitbucket-Issues:
[Bitbucket Issues](https://bitbucket.org/memo_development/contao-advisor-bundle/issues?status=new&status=open)

## License

[GNU Lesser General Public License, Version 3.0](https://www.gnu.de/documents/lgpl-3.0.de.html)


## Einbinden

- Seite anlegen für die Kundenberaterauflistung.
- Inhaltselement _Kundenberater nach Archiv_ in die Seite einfügen.
- Formular anlegen für die Kundenberatersuche mit der erstellten Seite als Weiterleitung.
- Textfeld anlegen mit der CSS-Klasse _findAdvisor_.
- Formular auf einer Seite einfügen.
