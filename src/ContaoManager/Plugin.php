<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\AdvisorBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\CalendarBundle\ContaoCalendarBundle;
use Contao\NewsBundle\ContaoNewsBundle;
use Memo\AdvisorBundle\MemoAdvisorBundle;
use Memo\CategoryBundle\MemoCategoryBundle;
use Memo\FoundationBundle\MemoFoundationBundle;

class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(MemoAdvisorBundle::class)
				->setLoadAfter([
					ContaoCoreBundle::class,
					ContaoCalendarBundle::class,
					ContaoNewsBundle::class,
					MemoCategoryBundle::class,
					MemoFoundationBundle::class
				])
				->setReplace(['advisor']),
        ];
    }
}
