<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\AdvisorBundle\EventListener;

use Terminal42\ChangeLanguage\Event\ChangelanguageNavigationEvent;
use Contao\CoreBundle\ServiceAnnotation\Hook;
use Memo\AdvisorBundle\Model\AdvisorModel;
use Memo\AdvisorBundle\Model\AdvisorArchiveModel;
use Memo\FoundationBundle\EventListener\FoundationHookListener;

class HookListener extends FoundationHookListener
{
	
}
