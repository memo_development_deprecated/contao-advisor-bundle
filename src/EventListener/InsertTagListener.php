<?php

declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\AdvisorBundle\EventListener;

use Memo\AdvisorBundle\Model\AdvisorArchiveModel;
use Memo\AdvisorBundle\Model\AdvisorModel;
use Contao\CoreBundle\ServiceAnnotation\Hook;

class InsertTagListener
{
	/**
	 * @Hook("replaceInsertTags")
	 */
	public function __invoke(
		string $insertTag,
		bool $useCache,
		string $cachedValue,
		array $flags,
		array $tags,
		array $cache,
		int $_rit,
		int $_cnt
	)
	{

	}
}
