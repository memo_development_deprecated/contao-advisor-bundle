<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\AdvisorBundle\FrontendModule;

use Memo\FoundationBundle\Module\FoundationModule;
use Memo\AdvisorBundle\Model\AdvisorModel;
use Memo\AdvisorBundle\Model\AdvisorContactModel;

class AdvisorListingArchive extends FoundationModule
{
	/**
	* Template
	* @var string
	*/

	protected $strTemplate = 'ce_advisor_listing';

	protected function compile()
	{
		if (TL_MODE == 'FE')
		{			
			//Generate Search-Form
			$objForm = new \Haste\Form\Form('searchForm', 'GET', function($objHaste){
				return \Input::post('FORM_SUBMIT') === $objHaste->getFormId();
			});
			
			
			$strGetSearch = \Input::get('search');
			if($strGetSearch && !str_contains($strGetSearch, "id-")) {
				$strValue = $strGetSearch;
			}
			elseif($strGetSearch && str_contains($strGetSearch, "id-")) {
				$objAdvisor = AdvisorModel::findBy(array("(tl_memo_advisor.start='' OR tl_memo_advisor.start<".time().")", "(tl_memo_advisor.stop='' OR tl_memo_advisor.stop>".time().")", "tl_memo_advisor.published='1'", "tl_memo_advisor.id=?"), str_replace("id-", "", $strGetSearch));
				
				if($objAdvisor) {
					$strGetSearch = $objAdvisor->id;
					$strValue = "";
					$blnOnlyId = true;
				}
			}
/*
			elseif(isset($_COOKIE['location'])){
				$objAdvisor = AdvisorModel::findBy(array("(tl_memo_advisor.start='' OR tl_memo_advisor.start<".time().")", "(tl_memo_advisor.stop='' OR tl_memo_advisor.stop>".time().")", "tl_memo_advisor.published='1'", "tl_memo_advisor.id=?"), $_COOKIE['location']);
				
				if($objAdvisor) {
					$strGetSearch = $objAdvisor->id;
					$strValue = "";
					$blnOnlyId = true;
				}
			}
*/
			
		    $objForm->addFormField('search', array(
		        'label'         => $GLOBALS["TL_LANG"]["MSC"]["search"],
		        'value'			=> $strValue,
		        'inputType'     => 'text'
		    ));
		    
		    $objForm->addSubmitFormField('', $GLOBALS["TL_LANG"]["MSC"]["submit"]);


		    $this->Template->objForm = $objForm;
		    
			// Get all selected Archives
			$arrArchives = unserialize( $this->foundation_archives );

			// Get override detail-page
			if($this->jumpTo)
			{
				$objDetailPage = \PageModel::findById($this->jumpTo);
			}

			// Define the findByOptions
			$strTable = AdvisorModel::getTable();
			$arrFindByOptions = $this->getFindByOptions($strTable);

			// Get entries
			if ((!empty($arrArchives) || is_array($arrArchives)) && !empty($strGetSearch))
			{		
				// Set Filter for Archives
				$arrColumns[] = "$strTable.pid IN(" . implode(',', array_map('\intval', $arrArchives)) . ")";
		
				// Set Filter for Published
				$arrColumns[] = "$strTable.published='1'";
		
				// Set Filter for stop/start
				$arrColumns[] = "($strTable.start='' OR $strTable.start<".time().")";
				$arrColumns[] = "($strTable.stop='' OR $strTable.stop>".time().")";
				if($blnOnlyId == true) {
					$arrColumns[] = "($strTable.id='$strGetSearch')";
					$colItems = AdvisorModel::findBy($arrColumns, NULL, $arrFindByOptions );
				}
				else {
					$arrColumns[] = "($strTable.keywords LIKE ?)";
					$colItems = AdvisorModel::findBy($arrColumns, array("%".$strGetSearch."%"), $arrFindByOptions );
				}
			}
			

			if( is_object($colItems) )
			{
				//Get AdvisorContact Informations
				$intCount = 0;
				foreach($colItems as $objItem) {
					$objItem->contactName = $objItem->getRelated("contact")->firstname." ".$objItem->getRelated("contact")->lastname;
					$objItem->contactAlias = $objItem->getRelated("contact")->alias;
					$intCount++;
				}
				
				$arrItems = $this->parseItems($colItems, $objDetailPage );
				$this->Template->items = $arrItems;
			
				//set Token
				if($intCount == 1) {
					setcookie("location", $colItems->id);
				}
			}
			
			//List all AdvisorContacts
			$collAdvisorContacts = AdvisorContactModel::findPublishedByPids($arrArchives);
			$objAdvisorContactsTemplate = new \FrontendTemplate("advisor_contact_listing");
			$objAdvisorContactsTemplate->collContacts = $collAdvisorContacts;
			if($intCount == 1) {
				$objAdvisorContactsTemplate->intAdvisorContactActive = $colItems->contact;
			}
			$objAdvisorContactsTemplate->size = $this->size;
			$this->Template->strAdvisorContacts = $objAdvisorContactsTemplate->parse();

			// Get custom Template
			if( $this->customTpl )
			{
				$this->Template->strTemplate = $this->customTpl;
			}

			// Parse Template
			$this->Template->parse();
		}
		else
		{
			$this->parseBackendTemplate();
		}
	}
}
