<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\AdvisorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MemoAdvisorBundle extends Bundle
{
}
