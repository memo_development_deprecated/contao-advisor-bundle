<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\AdvisorBundle\Model;
use Memo\FoundationBundle\Model\FoundationModel;

/**
 * Class AdvisorArchiveModel
 * @package Memo\AdvisorBundle\Model
 */
class AdvisorArchiveModel extends FoundationModel
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_memo_advisor_archive';

}
