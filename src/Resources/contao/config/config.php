<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Add back end modules
 */
array_insert($GLOBALS['BE_MOD']['content'], 100, array
(
	'advisor' => array
	(
		'tables'       => array(
			'tl_memo_advisor_archive',
			'tl_memo_advisor',
			'tl_memo_advisor_contact'
		),
		'table'       => array('Contao\TableWizard', 'importTable'),
	),
));

/**
 * Content elements
 */
$GLOBALS['TL_CTE']['advisor']['advisor_listing_archive']  = 'Memo\AdvisorBundle\FrontendModule\AdvisorListingArchive';;

/**
 * Add front end modules
 */

/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_memo_advisor_archive']          = 'Memo\AdvisorBundle\Model\AdvisorArchiveModel';
$GLOBALS['TL_MODELS']['tl_memo_advisor']                  = 'Memo\AdvisorBundle\Model\AdvisorModel';
$GLOBALS['TL_MODELS']['tl_memo_advisor_contact']          = 'Memo\AdvisorBundle\Model\AdvisorContactModel';

/**
 * AutoItem for Translation of aliases
 */
$GLOBALS['TL_AUTO_ITEM'][] = 'alias';

/**
 * Add permissions
 */
$GLOBALS['TL_PERMISSIONS'][] = 'advisors';
$GLOBALS['TL_PERMISSIONS'][] = 'advisorp';
