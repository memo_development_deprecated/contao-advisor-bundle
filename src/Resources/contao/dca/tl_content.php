<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

\Contao\Controller::loadDataContainer('tl_module');
\Contao\System::loadLanguageFile('tl_module');

/**
 * Table tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['palettes']['advisor_listing_archive'] = '{type_legend},type,headline; {source_legend}, size, foundation_archives, foundation_order; {template_legend:hide}, customTpl, foundation_item_template, ; {invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['advisor_listing_custom'] = '{type_legend},type,headline; {source_legend}, size, foundation_archives, foundation_item_selection; {template_legend:hide}, customTpl, foundation_item_template ; {invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_archives'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_archives'];
$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_archives']['options_callback'] = array('tl_content_advisor', 'getArchives');
$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_archives']['eval']['submitOnChange'] = true;

$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_item_template'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_template'];
$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_item_template']['options_callback'] = array('tl_content_advisor', 'getTemplates');
$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_item_selection'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_selection'];

$GLOBALS['TL_DCA']['tl_content']['fields']['foundation_order'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_order'];

use Memo\AdvisorBundle\Model\AdvisorModel;
use Memo\AdvisorBundle\Model\AdvisorArchiveModel;

/**
 * Class tl_content
 * Definition der Callback-Funktionen für das Datengefäss.
 */
class tl_content_advisor extends Backend
{
	public function getArchives(Contao\DataContainer $dc)
	{
		$arrArchives = array();
		$colArchives = AdvisorArchiveModel::findAll();

		while ($colArchives->next())
		{
			$arrArchives[$colArchives->id] = $colArchives->title;
		}

		return $arrArchives;
	}

	public function getTemplates(Contao\DataContainer $dc)
	{
		return $this->getTemplateGroup('advisor_');
	}
}

