<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Memo\MemoFoundationBundle\Classes\FoundationBackend;
use Memo\AdvisorBundle\Model\AdvisorModel;

/**
 * Table tl_memo_advisor
 */
$GLOBALS['TL_DCA']['tl_memo_advisor'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'					=> 'Table',
		'ptable'						=> 'tl_memo_advisor_archive',
		'switchToEdit'					=> true,
		'enableVersioning'				=> true,
		'markAsCopy'					=> 'title',
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'pid,sorting' => 'index'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'						=> 1,
			'flag'						=> 3,
			'fields'					=> array('canton'),
			'panelLayout'				=> 'filter;search;sort,limit',
			'headerFields'				=> array('title', 'alias'),
			'disableGrouping'			=> false
		),
		'label' => array
		(
			'fields'					=> array('zip', 'location'),
			'format'					=> '%s %s',
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'					=> 'act=select',
				'class'					=> 'header_edit_all',
				'attributes'			=> 'onclick="Backend.getScrollOffset();" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['edit'],
				'href'					=> 'act=edit',
				'icon'					=> 'edit.gif'
			),
			'copy' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['copy'],
				'href'					=> 'act=copy',
				'icon'					=> 'copy.gif'
			),
			'delete' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['delete'],
				'href'					=> 'act=delete',
				'icon'					=> 'delete.gif',
				'attributes'			=> 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'toggle' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['toggle'],
				'icon'					=> 'visible.gif',
				'attributes'			=> 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
				'button_callback'		=> array(
					'tl_memo_advisor', 'toggleIcon'
				)
			),
			'show' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['show'],
				'href'					=> 'act=show',
				'icon'					=> 'show.gif'
			),
		)
	),

	// Select
	'select' => array
	(
		'buttons_callback' => array()
	),

	// Edit
	'edit' => array
	(
		'buttons_callback' => array()
	),

	// Palettes
	'palettes' => array
	(
		'default'						=> '
			{general_legend}, location, alias, canton, zip, contact, keywords;
			{publish_legend}, published, start, stop;',
	),

	// Subpalettes
	'subpalettes' => array
	(
		''					=> '',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'						=> "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
			'foreignKey'				=> 'tl_memo_advisor_archive.id',
			'sql'						=> "int(10) unsigned NOT NULL",
			'relation'					=> array('type'=>'belongsTo', 'load'=>'lazy')
		),
		'sorting' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['MSC']['sorting'],
			'sorting'					=> false,
			'sql'						=> "int(10) unsigned NOT NULL default '0'"
		),
		'tstamp' => array
		(
			'sql'						=> "int(10) unsigned NOT NULL default '0'"
		),
		'canton' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['canton'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> false,
			'sorting'					=> true,
			'inputType'					=> 'text',
			'eval'						=> array(
				'mandatory'=>true,
				'maxlength' => 255,
				'tl_class' => 'w50'
			),
			'sql'						=> "varchar(255) NOT NULL default ''",
		),
		'location' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['location'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> true,
			'sorting'					=> false,
			'inputType'					=> 'text',
			'eval'						=> array(
				'mandatory'=>true,
				'maxlength' => 255,
				'tl_class' => 'w50'
			),
			'sql'						=> "varchar(255) NOT NULL default ''",
		),
		'zip' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['zip'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> true,
			'sorting'					=> true,
			'inputType'					=> 'text',
			'eval'						=> array(
				'mandatory'=>true,
				'maxlength' => 255,
				'tl_class' => 'w50'
			),
			'sql'						=> "varchar(128) COLLATE utf8_bin NOT NULL default ''"
		),
		'alias' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['alias'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> true,
			'sorting'					=> false,
			'inputType'					=> 'text',
			'eval'						=> array(
				'rgxp'=>'alias',
				'doNotCopy'=>true,
				'maxlength'=>128,
				'tl_class'=>'w50',
				'unique'=>true
			),
			'save_callback' 			=> array
			(
				array('tl_memo_advisor', 'generateAlias')
			),
			'sql'						=> "varchar(128) COLLATE utf8_bin NOT NULL default ''"
		),
		'contact' => array
		(
			'label' => &$GLOBALS['TL_LANG']['tl_memo_advisor']['contact'],
			'exclude' 					=> true,
			'filter' 					=> true,
			'inputType' 				=> 'select',
			'foreignKey' 				=> 'tl_memo_advisor_contact.lastname',
			'relation'                	=> array('type'=>'hasOne', 'load'=>'eager'),
			'eval' 						=> array('mandatory' => true, 'tl_class'=>'w50'),
			'sql'						=> "varchar(255) NOT NULL default ''",
		),
		'keywords' => array
		(
			'label'               	  => &$GLOBALS['TL_LANG']['tl_memo_advisor']['keywords'],
			'exclude'             	  => true,
			'filter'              	  => false,
			'search'                  => true,
			'sorting'                 => true,
			'inputType'           	  => 'textarea',
			'eval'                    => array('mandatory'=>false, 'maxlength' => 255, 'tl_class' => 'clr'),
			'save_callback' => array
			(
				array('tl_memo_advisor', 'generateKeywords')
			),
			'sql'                 	  => "varchar(255) NOT NULL default ''",
		),
		'published' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['published'],
			'exclude'					=> true,
			'filter'					=> true,
			'search'					=> false,
			'sorting'					=> false,
			'inputType'					=> 'checkbox',
			'eval'						=> array(
				'doNotCopy'=>true,
				'tl_class'=>'w50 clr'
			),
			'sql'						=> "char(1) NOT NULL default ''",
		),
		'start' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['start'],
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> false,
			'sorting'					=> false,
			'inputType'					=> 'text',
			'eval'						=> array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard clr'),
			'sql'						=> "varchar(10) NOT NULL default ''"
		),
		'stop' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['stop'],
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> false,
			'sorting'					=> false,
			'inputType'					=> 'text',
			'eval'						=> array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'						=> "varchar(10) NOT NULL default ''"
		)
	)
);

// Enable a hook to be loaded here
if (isset($GLOBALS['TL_HOOKS']['generateAdvisorDCA']) && \is_array($GLOBALS['TL_HOOKS']['generateAdvisorDCA']))
{
	foreach ($GLOBALS['TL_HOOKS']['generateAdvisorDCA'] as $callback)
	{
		$this->import($callback[0]);
		$GLOBALS['TL_DCA']['tl_memo_advisor'] = $this->{$callback[0]}->{$callback[1]}($GLOBALS['TL_DCA']['tl_memo_advisor'], $this);
	}
}

// Add multi-lang fields automatically by checking for the translate field
$objLanguageService = \System::getContainer()->get('memo.foundation.language');
$objLanguageService->generateTranslateDCA('tl_memo_advisor');

// Enable a hook to be loaded here
if (isset($GLOBALS['TL_HOOKS']['generateAdvisorDCAComplete']) && \is_array($GLOBALS['TL_HOOKS']['generateAdvisorDCAComplete']))
{
	foreach ($GLOBALS['TL_HOOKS']['generateAdvisorDCAComplete'] as $callback)
	{
		$this->import($callback[0]);
		$GLOBALS['TL_DCA']['tl_memo_advisor'] = $this->{$callback[0]}->{$callback[1]}($GLOBALS['TL_DCA']['tl_memo_advisor'], $this);
	}
}

/**
 * Class tl_memo_advisor
 */
class tl_memo_advisor extends FoundationBackend
{

	public function listLayout($row)
	{
		return '<div class="tl_content_left">'. '<strong>' . $row['title'] . '</strong></div>';
	}

	public function generateAlias($varValue, DataContainer $dc)
	{
		return $this->generateAliasFoundation($dc, $varValue, 'tl_memo_advisor', array('location'));
	}

	public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
	{
		return $this->toggleIconFoundation($row, 'published', 'invisible', $icon, $title, $attributes, $label);
	}

	public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
	{
		$this->toggleVisibilityFoundation($intId, $blnVisible, $dc, 'tl_memo_advisor', 'published');
	}
	
	public function onContactOptionsCallback($dc)
	{
		$objEntry = AdvisorModel::findOneBy('id', $dc->activeRecord->id);
		$intCategoryPid = $objEntry->pid;
		$colContacts = \Memo\AdvisorBundle\Model\AdvisorContactModel::findBy('pid', $intCategoryPid);
	
		$arrOptions = [];
	
		if( $colContacts != null )
		{
			foreach( $colContacts as $objContact )
			{
				if( $objContact->langPid )
				{
					$arrOptions[$objContact->langPid] = $objContact->firstname." ".$objContact->lastname;
				} else {
					$arrOptions[$objContact->id] = $objContact->firstname." ".$objContact->lastname;
				}
			}
	
		}
	
		return $arrOptions;
	}
	
	public function generateKeywords($varValue, DataContainer $dc)
	{
		
		$objModel = AdvisorModel::findById($dc->activeRecord->id);
		
		$strKeywords = "";
		$strKeywords .= $objModel->location;
		$strKeywords .= ", ".$objModel->zip;
		$strKeywords .= ", ".$objModel->zip." ".$objModel->location;
		
		$objModel->keywords = $strKeywords;
		$objModel->save();
		
		return $varValue;
	}
}
