<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Memo\MemoFoundationBundle\Classes\FoundationBackend;
use Contao\BackendUser;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Table tl_memo_advisor_archive
 */
$GLOBALS['TL_DCA']['tl_memo_advisor_archive'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'				=> 'Table',
		'enableVersioning'			=> true,
		'ctable'					=> array(
			'tl_memo_advisor',
			'tl_memo_advisor_contact'
		),
		'switchToEdit'				=> true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary'
			)
		),
		'onload_callback' => array
		(
			array('tl_memo_advisor_archive', 'checkPermission')
		),
		'oncreate_callback' => array
		(
			array('tl_memo_advisor_archive', 'adjustPermissions')
		),
		'oncopy_callback' => array
		(
			array('tl_memo_advisor_archive', 'adjustPermissions')
		),
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'					=> 1,
			'fields'				=> array('title'),
			'flag'					=> 1,
			'panelLayout'			=> 'filter; search; limit'
		),
		'label' => array
		(
			'fields'				=> array('title', 'alias'),
			'format'				=> '%s <span class="backend-info">[%s]</span>'
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'				=> &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'				=> 'act=select',
				'class'				=> 'header_edit_all',
				'attributes'		=> 'onclick="Backend.getScrollOffset();" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'				=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['edit'],
				'href'				=> 'table=tl_memo_advisor',
				'icon'				=> 'edit.gif'
			),
			'editheader' => array
			(
				'label'				=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['editheader'],
				'href'				=> 'act=edit',
				'icon'				=> 'header.gif',
				'button_callback'	=> array(
					'tl_memo_advisor_archive',
					'editProjectHeader'
				),
				'attributes'		=> 'class="edit-header"'
			),
			'editContact' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['editContact'],
				'href'                => 'table=tl_memo_advisor_contact',
				'icon'                => 'article.gif',
				'button_callback'     => array('tl_memo_advisor_archive', 'editAdvisorContacts'),
				'attributes'          => 'class="edit-contacts"'
			),
			'copy' => array
			(
				'label'				=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['copy'],
				'href'				=> 'act=copy',
				'icon'				=> 'copy.svg',
				'button_callback'	=> array('tl_memo_advisor_archive', 'copyArchive')
			),
			'delete' => array
			(
				'label'				=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['delete'],
				'href'				=> 'act=delete',
				'icon'				=> 'delete.svg',
				'attributes'		=> 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"',
				'button_callback'	=> array('tl_memo_advisor_archive', 'deleteArchive')
			),
			'toggle' => array
			(
				'label'				=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['toggle'],
				'icon'				=> 'visible.gif',
				'attributes'		=> 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
				'button_callback'	=> array(
					'tl_memo_advisor_archive',
					'toggleIcon'
				)
			),
			'show' => array
			(
				'label'				=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['show'],
				'href'				=> 'act=show',
				'icon'				=> 'show.gif'
			),
		)
	),

	// Select
	'select' => array
	(
		'buttons_callback' => array()
	),

	// Edit
	'edit' => array
	(
		'buttons_callback' => array()
	),

	// Palettes
	'palettes' => array
	(
		'default'					=> '{general_legend}, title, alias, jumpTo, jumpToListing, teaser; {media_legend}, singleSRC; {publish_legend}, published, start, stop;',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'					=> "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'					=> "int(10) unsigned NOT NULL default '0'"
		),
		'title' => array
		(
			'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['title'],
			'translate'				=> true,
			'exclude'				=> true,
			'filter'				=> false,
			'search'				=> true,
			'sorting'				=> true,
			'inputType'				=> 'text',
			'eval'					=> array(
				'mandatory'=>true,
				'maxlength' => 255,
				'tl_class' => 'w50'
			),
			'sql'					=> "varchar(255) NOT NULL default ''",
		),
		'alias' => array
		(
			'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['alias'],
			'translate'				=> true,
			'exclude'				=> true,
			'filter'				=> false,
			'search'				=> true,
			'sorting'				=> false,
			'inputType'				=> 'text',
			'eval'					=> array(
				'rgxp'=>'alias',
				'doNotCopy'=>true,
				'maxlength'=>128,
				'tl_class'=>'w50'
			),
			'save_callback' => array
			(
				array('tl_memo_advisor_archive', 'generateAlias')
			),
			'sql'					=> "varchar(128) COLLATE utf8_bin NOT NULL default ''"
		),
		'jumpTo' => array
		(
			'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['jumpTo'],
			'translate'				=> true,
			'exclude'				=> true,
			'inputType'				=> 'pageTree',
			'foreignKey'			=> 'tl_page.title',
			'eval'					=> array(
				'fieldType' => 'radio',
				'tl_class' => 'clr m12'
			),
			'sql'					=> "int(10) unsigned NOT NULL default 0",
			'relation'				=> array(
				'type'=>'hasOne',
				'load'=>'lazy'
			)
		),
		'jumpToListing' => array
		(
			'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['jumpToListing'],
			'translate'				=> true,
			'exclude'				=> true,
			'inputType'				=> 'pageTree',
			'foreignKey'			=> 'tl_page.title',
			'eval'					=> array(
				'fieldType' => 'radio',
				'tl_class' => 'clr'
			),
			'sql'					=> "int(10) unsigned NOT NULL default 0",
			'relation'				=> array(
				'type'=>'hasOne',
				'load'=>'lazy'
			)
		),
		'teaser' => array
		(
			'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['description'],
			'translate'				=> true,
			'exclude'				=> true,
			'filter'				=> false,
			'search'				=> false,
			'sorting'				=> false,
			'inputType'				=> 'textarea',
			'eval'					=> array(
				'mandatory' => false,
				'rte' => 'tinyMCE',
				'tl_class' => 'clr',
			),
			'sql'					=> "mediumtext NULL"
		),
		'singleSRC' => array
		(
			'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['singleSRC'],
			'translate'				=> false,
			'exclude'				=> true,
			'filter'				=> false,
			'search'				=> false,
			'sorting'				=> false,
			'inputType'				=> 'fileTree',
			'eval'					=> array(
				'filesOnly'=>true,
				'extensions'=>Config::get('validImageTypes'),
				'fieldType'=>'radio'
			),
			'sql'					=> "binary(16) NULL"
		),
		'published' => array
		(
			'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['published'],
			'translate'				=> false,
			'exclude'				=> true,
			'filter'				=> true,
			'search'				=> false,
			'sorting'				=> false,
			'inputType'				=> 'checkbox',
			'eval'					=> array(
				'doNotCopy'=>true,
				'tl_class'=>'w50'
			),
			'sql'					=> "char(1) NOT NULL default ''",
		),
		'start' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['start'],
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> false,
			'sorting'					=> false,
			'inputType'					=> 'text',
			'eval'						=> array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard clr'),
			'sql'						=> "varchar(10) NOT NULL default ''"
		),
		'stop' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['stop'],
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> false,
			'sorting'					=> false,
			'inputType'					=> 'text',
			'eval'						=> array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'						=> "varchar(10) NOT NULL default ''"
		)
	)
);

// Add the translated-fields for all 'translate' dca-fields
$objLanguageService = \System::getContainer()->get('memo.foundation.language');
$objLanguageService->generateTranslateDCA('tl_memo_advisor_archive');

/**
 * Class tl_memo_advisor_archive
 */
class tl_memo_advisor_archive extends FoundationBackend
{
	public function __construct()
	{
		parent::__construct();
		$this->import(BackendUser::class, 'User');
	}

	/**
	 * Check permissions to edit table tl_memo_advisor_archive
	 *
	 * @throws AccessDeniedException
	 */
	public function checkPermission()
	{

		// Allow Admins
		if ($this->User->isAdmin)
		{
			return;
		}

		// Set root IDs
		if (empty($this->User->advisors) || !is_array($this->User->advisors))
		{
			$root = array(0);
		}
		else
		{
			$root = $this->User->advisors;
		}

		// Get Group Permissions manually
		if(is_array($this->User->groups) && is_array($this->User->groups) && count($this->User->groups) > 0){

			foreach($this->User->groups as $intGroupID){

				$objGroup = UserGroupModel::findByPk($intGroupID);

				if($objGroup && $objGroup->advisors){

					$arrAdvisors = unserialize($objGroup->advisors);

					if(is_array($arrAdvisors) && count($arrAdvisors) > 0){
						foreach($arrAdvisors as $intArchive){
							if(!in_array($intArchive, $root)){
								$root[] = $intArchive;
							}
						}
					}
				}

			}

		}

		$GLOBALS['TL_DCA']['tl_memo_advisor_archive']['list']['sorting']['root'] = $root;

		// Check permissions to add archives
		if (!$this->User->hasAccess('create', 'advisorp'))
		{
			$GLOBALS['TL_DCA']['tl_memo_advisor_archive']['config']['closed'] = true;
			$GLOBALS['TL_DCA']['tl_memo_advisor_archive']['config']['notCreatable'] = true;
			$GLOBALS['TL_DCA']['tl_memo_advisor_archive']['config']['notCopyable'] = true;
		}



		// Check permissions to delete calendars
		if (!$this->User->hasAccess('delete', 'advisorp'))
		{
			$GLOBALS['TL_DCA']['tl_memo_advisor_archive']['config']['notDeletable'] = true;
		}

		/** @var SessionInterface $objSession */
		$objSession = System::getContainer()->get('session');

		// Check current action
		switch (Input::get('act'))
		{
			case 'select':
				// Allow
				break;

			case 'create':
				if (!$this->User->hasAccess('create', 'advisorp'))
				{
					throw new AccessDeniedException('Not enough permissions to create advisor archives.');
				}
				break;

			case 'edit':
			case 'copy':
			case 'delete':
			case 'show':
				if (!in_array(Input::get('id'), $root) || (Input::get('act') == 'delete' && !$this->User->hasAccess('delete', 'advisorp')))
				{
					throw new AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' advisor archive ID ' . Input::get('id') . '.');
				}
				break;

			case 'editAll':
			case 'deleteAll':
			case 'overrideAll':
			case 'copyAll':
				$session = $objSession->all();

				if (Input::get('act') == 'deleteAll' && !$this->User->hasAccess('delete', 'advisorp'))
				{
					$session['CURRENT']['IDS'] = array();
				}
				else
				{
					$session['CURRENT']['IDS'] = array_intersect((array) $session['CURRENT']['IDS'], $root);
				}
				$objSession->replace($session);
				break;

			default:
				if (Input::get('act'))
				{
					throw new AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' advisors archives.');
				}
				break;
		}
	}

	/**
	 * Add the new archive to the permissions
	 *
	 * @param $insertId
	 */
	public function adjustPermissions($insertId)
	{
		// The oncreate_callback passes $insertId as second argument
		if (func_num_args() == 4)
		{
			$insertId = func_get_arg(1);
		}

		if ($this->User->isAdmin)
		{
			return;
		}

		// Set root IDs
		if (empty($this->User->advisors) || !is_array($this->User->advisors))
		{
			$root = array(0);
		}
		else
		{
			$root = $this->User->advisors;
		}

		// The archive is enabled already
		if (in_array($insertId, $root))
		{
			return;
		}

		/** @var AttributeBagInterface $objSessionBag */
		$objSessionBag = System::getContainer()->get('session')->getBag('contao_backend');

		$arrNew = $objSessionBag->get('new_records');

		if (is_array($arrNew['tl_memo_advisor_archive']) && in_array($insertId, $arrNew['tl_memo_advisor_archive']))
		{
			// Add the permissions on group level
			if ($this->User->inherit != 'custom')
			{
				$objGroup = $this->Database->execute("SELECT id, advisors, advisorp FROM tl_user_group WHERE id IN(" . implode(',', array_map('\intval', $this->User->groups)) . ")");

				while ($objGroup->next())
				{
					$arrAdvisorp = StringUtil::deserialize($objGroup->advisorp);

					if (is_array($arrAdvisorp) && in_array('create', $arrAdvisorp))
					{
						$arrAdvisors = StringUtil::deserialize($objGroup->advisors, true);
						$arrAdvisors[] = $insertId;

						$this->Database->prepare("UPDATE tl_user_group SET advisors=? WHERE id=?")
							->execute(serialize($arrAdvisors), $objGroup->id);
					}
				}
			}

			// Add the permissions on user level
			if ($this->User->inherit != 'group')
			{
				$objUser = $this->Database->prepare("SELECT advisors, advisorp FROM tl_user WHERE id=?")
					->limit(1)
					->execute($this->User->id);

				$arrAdvisorp = StringUtil::deserialize($objUser->advisorp);

				if (is_array($arrAdvisorp) && in_array('create', $arrAdvisorp))
				{
					$arrAdvisors = StringUtil::deserialize($objUser->advisors, true);
					$arrAdvisors[] = $insertId;

					$this->Database->prepare("UPDATE tl_user SET advisors=? WHERE id=?")
						->execute(serialize($arrAdvisors), $this->User->id);
				}
			}

			// Add the new element to the user object
			$root[] = $insertId;
			$this->User->advisors = $root;
		}
	}

	/**
	 * Return the copy archive button
	 *
	 * @param array  $row
	 * @param string $href
	 * @param string $label
	 * @param string $title
	 * @param string $icon
	 * @param string $attributes
	 *
	 * @return string
	 */
	public function copyArchive($row, $href, $label, $title, $icon, $attributes)
	{
		return $this->User->hasAccess('create', 'advisorp') ? '<a href="' . $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)) . ' ';
	}

	/**
	 * Return the delete archive button
	 *
	 * @param array  $row
	 * @param string $href
	 * @param string $label
	 * @param string $title
	 * @param string $icon
	 * @param string $attributes
	 *
	 * @return string
	 */
	public function deleteArchive($row, $href, $label, $title, $icon, $attributes)
	{
		return $this->User->hasAccess('delete', 'advisorp') ? '<a href="' . $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label) . '</a> ' : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)) . ' ';
	}

	public function editProjectHeader($row, $href, $label, $title, $icon, $attributes)
	{
		return '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'.Contao\StringUtil::specialchars($title).'"'.$attributes.'>'.Contao\Image::getHtml($icon, $label).'</a> ';
	}

	public function generateAlias($varValue, DataContainer $dc)
	{
		return $this->generateAliasFoundation($dc, $varValue, 'tl_memo_advisor_archive');
	}

	public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
	{
		return $this->toggleIconFoundation($row, 'published', 'invisible', $icon, $title, $attributes, $label);
	}

	public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
	{
		$this->toggleVisibilityFoundation($intId, $blnVisible, $dc, 'tl_memo_advisor_archive', 'published');
	}
	
	public function editAdvisorContacts($row, $href, $label, $title, $icon, $attributes)
	{
		return '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'.Contao\StringUtil::specialchars($title).'"'.$attributes.'>'.Contao\Image::getHtml($icon, $label).'</a> ';
	}

}
