<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Memo\MemoFoundationBundle\Classes\FoundationBackend;
use Memo\AdvisorBundle\Model\AdvisorModel;

/**
 * Table tl_memo_advisor_contact
 */
$GLOBALS['TL_DCA']['tl_memo_advisor_contact'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'					=> 'Table',
		'ptable'						=> 'tl_memo_advisor_archive',
		'switchToEdit'					=> true,
		'enableVersioning'				=> true,
		'markAsCopy'					=> 'title',
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'pid,sorting' => 'index'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'						=> 0,
			'flag'						=> 8,
			'fields'					=> array('firstname', 'lastname'),
			'panelLayout'				=> 'filter;search;sort,limit',
			'headerFields'				=> array('firstname', 'lastname'),
			'disableGrouping'			=> false
		),
		'label' => array
		(
			'fields'					=> array('firstname', 'lastname'),
			'format'					=> '%s %s',
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'					=> 'act=select',
				'class'					=> 'header_edit_all',
				'attributes'			=> 'onclick="Backend.getScrollOffset();" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['edit'],
				'href'					=> 'act=edit',
				'icon'					=> 'edit.gif'
			),
			'copy' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['copy'],
				'href'					=> 'act=copy',
				'icon'					=> 'copy.gif'
			),
			'delete' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['delete'],
				'href'					=> 'act=delete',
				'icon'					=> 'delete.gif',
				'attributes'			=> 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'toggle' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['toggle'],
				'icon'					=> 'visible.gif',
				'attributes'			=> 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
				'button_callback'		=> array(
					'tl_memo_advisor_contact', 'toggleIcon'
				)
			),
			'show' => array
			(
				'label'					=> &$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['show'],
				'href'					=> 'act=show',
				'icon'					=> 'show.gif'
			),
		)
	),

	// Select
	'select' => array
	(
		'buttons_callback' => array()
	),

	// Edit
	'edit' => array
	(
		'buttons_callback' => array()
	),

	// Palettes
	'palettes' => array
	(
		'default'						=> '
			{general_legend}, firstname, lastname, alias, function, location, tel, mobile, email, singleSRC;
			{publish_legend}, published, start, stop;',
	),

	// Subpalettes
	'subpalettes' => array
	(
		''					=> '',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'						=> "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
			'foreignKey'				=> 'tl_memo_advisor_archive.id',
			'sql'						=> "int(10) unsigned NOT NULL",
			'relation'					=> array('type'=>'belongsTo', 'load'=>'lazy')
		),
		'sorting' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['MSC']['sorting'],
			'sorting'					=> false,
			'sql'						=> "int(10) unsigned NOT NULL default '0'"
		),
		'tstamp' => array
		(
			'sql'						=> "int(10) unsigned NOT NULL default '0'"
		),
		'firstname' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['firstname'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> true,
			'sorting'					=> true,
			'inputType'					=> 'text',
			'eval'						=> array(
				'mandatory'=>true,
				'maxlength' => 128,
				'tl_class' => 'w50'
			),
			'sql'						=> "varchar(128) NOT NULL default ''",
		),
		'lastname' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['lastname'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> true,
			'sorting'					=> true,
			'inputType'					=> 'text',
			'eval'						=> array(
				'mandatory'=>true,
				'maxlength' => 128,
				'tl_class' => 'w50'
			),
			'sql'						=> "varchar(128) NOT NULL default ''",
		),
		'alias' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['alias'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> true,
			'sorting'					=> false,
			'inputType'					=> 'text',
			'eval'						=> array(
				'rgxp'=>'alias',
				'doNotCopy'=>true,
				'maxlength'=>128,
				'tl_class'=>'w50',
				'unique'=>true
			),
			'save_callback' 			=> array
			(
				array('tl_memo_advisor_contact', 'generateAlias')
			),
			'sql'						=> "varchar(128) COLLATE utf8_bin NOT NULL default ''"
		),
		'function' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['function'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> true,
			'sorting'					=> true,
			'inputType'					=> 'text',
			'eval'						=> array(
				'mandatory'=>true,
				'maxlength' => 255,
				'tl_class' => 'w50'
			),
			'sql'						=> "varchar(255) NOT NULL default ''",
		),
		'location' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['location'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> false,
			'sorting'					=> false,
			'inputType'					=> 'text',
			'eval'						=> array(
				'mandatory'=>false,
				'maxlength' => 255,
				'tl_class' => 'w50'
			),
			'sql'						=> "varchar(255) NOT NULL default ''",
		),
		'tel' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['tel'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> true,
			'sorting'					=> true,
			'inputType'					=> 'text',
			'eval'						=> array(
				'mandatory'=>false,
				'maxlength' => 128,
				'tl_class' => 'w50'
			),
			'sql'						=> "varchar(128) COLLATE utf8_bin NOT NULL default ''"
		),
		'mobile' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['mobile'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> true,
			'sorting'					=> true,
			'inputType'					=> 'text',
			'eval'						=> array(
				'mandatory'=>false,
				'maxlength' => 128,
				'tl_class' => 'w50'
			),
			'sql'						=> "varchar(128) COLLATE utf8_bin NOT NULL default ''"
		),
		'email' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['email'],
			'translate'					=> false,
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> true,
			'sorting'					=> true,
			'inputType'					=> 'text',
			'eval'						=> array(
				'mandatory'=>false,
				'maxlength' => 128,
				'tl_class' => 'w50'
			),
			'sql'						=> "varchar(128) COLLATE utf8_bin NOT NULL default ''"
		),
		'singleSRC' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['singleSRC'],
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> false,
			'sorting'					=> false,
			'inputType'					=> 'fileTree',
			'eval'						=> array(
				'filesOnly' => true,
				'extensions' => Config::get('validImageTypes'),
				'fieldType' => 'radio',
				'tl_class' => 'clr'),
			'sql'						=> "binary(16) NULL"
		),
		'published' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['published'],
			'exclude'					=> true,
			'filter'					=> true,
			'search'					=> false,
			'sorting'					=> false,
			'inputType'					=> 'checkbox',
			'eval'						=> array(
				'doNotCopy'=>true,
				'tl_class'=>'w50 clr'
			),
			'sql'						=> "char(1) NOT NULL default ''",
		),
		'start' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['start'],
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> false,
			'sorting'					=> false,
			'inputType'					=> 'text',
			'eval'						=> array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard clr'),
			'sql'						=> "varchar(10) NOT NULL default ''"
		),
		'stop' => array
		(
			'label'						=> &$GLOBALS['TL_LANG']['tl_memo_advisor']['stop'],
			'exclude'					=> true,
			'filter'					=> false,
			'search'					=> false,
			'sorting'					=> false,
			'inputType'					=> 'text',
			'eval'						=> array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'						=> "varchar(10) NOT NULL default ''"
		)
	)
);

// Enable a hook to be loaded here
if (isset($GLOBALS['TL_HOOKS']['generateAdvisorDCA']) && \is_array($GLOBALS['TL_HOOKS']['generateAdvisorDCA']))
{
	foreach ($GLOBALS['TL_HOOKS']['generateAdvisorDCA'] as $callback)
	{
		$this->import($callback[0]);
		$GLOBALS['TL_DCA']['tl_memo_advisor_contact'] = $this->{$callback[0]}->{$callback[1]}($GLOBALS['TL_DCA']['tl_memo_advisor_contact'], $this);
	}
}

// Add multi-lang fields automatically by checking for the translate field
$objLanguageService = \System::getContainer()->get('memo.foundation.language');
$objLanguageService->generateTranslateDCA('tl_memo_advisor_contact');

// Enable a hook to be loaded here
if (isset($GLOBALS['TL_HOOKS']['generateAdvisorDCAComplete']) && \is_array($GLOBALS['TL_HOOKS']['generateAdvisorDCAComplete']))
{
	foreach ($GLOBALS['TL_HOOKS']['generateAdvisorDCAComplete'] as $callback)
	{
		$this->import($callback[0]);
		$GLOBALS['TL_DCA']['tl_memo_advisor_contact'] = $this->{$callback[0]}->{$callback[1]}($GLOBALS['TL_DCA']['tl_memo_advisor_contact'], $this);
	}
}

/**
 * Class tl_memo_advisor_contact
 */
class tl_memo_advisor_contact extends FoundationBackend
{

	public function listLayout($row)
	{
		return '<div class="tl_content_left">'. '<strong>' . $row['title'] . '</strong></div>';
	}

	public function generateAlias($varValue, DataContainer $dc)
	{
		return $this->generateAliasFoundation($dc, $varValue, 'tl_memo_advisor_contact', array('firstname', 'lastname'));
	}

	public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
	{
		return $this->toggleIconFoundation($row, 'published', 'invisible', $icon, $title, $attributes, $label);
	}

	public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
	{
		$this->toggleVisibilityFoundation($intId, $blnVisible, $dc, 'tl_memo_advisor_contact', 'published');
	}
}
