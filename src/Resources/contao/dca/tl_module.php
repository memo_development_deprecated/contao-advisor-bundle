<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Memo\AdvisorBundle\Model\AdvisorArchiveModel;
use Memo\AdvisorBundle\Model\AdvisorModel;

/**
 * Add palettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['advisor_listing'] = '{title_legend}, name, type;{config_legend}, imgSize, imgSizeGallery, foundation_archives, foundation_order, categories, jumpTo;{template_legend}, customTpl, foundation_item_template;{expert_legend:hide},cssID';
$GLOBALS['TL_DCA']['tl_module']['palettes']['advisor_reader'] = '{title_legend}, name, type;{config_legend}, imgSize, imgSizeGallery;{template_legend}, customTpl, foundation_item_template;{expert_legend:hide},cssID';

$GLOBALS['TL_DCA']['tl_module']['fields']['jumpTo']['eval']['tl_class'] = 'w50 clr';
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpTo']['label'] = &$GLOBALS['TL_LANG']['tl_module']['jumpToOverride'];

$GLOBALS['TL_DCA']['tl_module']['config']['onload_callback'][] = array('tl_module_advisor', 'autoOverrideOptions');

/**
 * Class tl_module_advisor
 */
class tl_module_advisor extends Backend {

	public function autoOverrideOptions($dc)
	{
		$objModuleItem = \Contao\ModuleModel::findByPk($dc->id);

		if(!is_null($objModuleItem->type) && stristr($objModuleItem->type, 'advisor')){
			
			$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_archives']['options_callback'] = array('tl_module_advisor', 'getArchives');
			
			$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_template']['options_callback'] = array('tl_module_advisor', 'getTemplates');

			$GLOBALS['TL_DCA']['tl_module']['fields']['foundation_item_selection']['options_callback'] = array('tl_module_advisor', 'getSelectionOptions');
		}

	}

	public function getArchives(Contao\DataContainer $dc)
	{
		$arrArchives = array();
		$colArchives = AdvisorArchiveModel::findAll();

		while ($colArchives->next())
		{
			$arrArchives[$colArchives->id] = $colArchives->title;
		}

		return $arrArchives;
	}

	public function getSelectionOptions(Contao\DataContainer $dc)
	{
		// If multiple, unserialize
		if( is_string( $dc->activeRecord->foundation_archives ) )
		{
			$arrArchives = unserialize( $dc->activeRecord->foundation_archives );
		} else {
			$arrArchives = $dc->activeRecord->foundation_archives;
		}

		// Get all Items
		$arrItems = array();
		if ( !empty( $arrArchives) )
		{
			$colItems = AdvisorModel::findPublishedByPids( $arrArchives );

			if( is_object( $colItems ) )
			{
				foreach( $colItems as $objItem )
				{
					$arrItems[$objItem->id] = $objItem->title;
				}
			}
		}

		return $arrItems;
	}

	public function getTemplates(Contao\DataContainer $dc)
	{
		return $this->getTemplateGroup('advisor_');
	}
}
