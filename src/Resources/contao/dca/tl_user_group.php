<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;

// Extend the default palette
PaletteManipulator::create()
->addLegend('advisor_legend', 'amg_legend', PaletteManipulator::POSITION_BEFORE)
->addField(array('advisors', 'advisorp'), 'advisor_legend', PaletteManipulator::POSITION_APPEND)
->applyToPalette('default', 'tl_user_group')
;

// Add fields to tl_user_group
$GLOBALS['TL_DCA']['tl_user_group']['fields']['advisors'] = array
(
'label'                   => &$GLOBALS['TL_LANG']['tl_user']['advisors'],
'exclude'                 => true,
'inputType'               => 'checkbox',
'foreignKey'              => 'tl_memo_advisor_archive.title',
'eval'                    => array('multiple'=>true),
'sql'                     => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_user_group']['fields']['advisorp'] = array
(
'label'                   => &$GLOBALS['TL_LANG']['tl_user']['advisorp'],
'exclude'                 => true,
'inputType'               => 'checkbox',
'options'                 => array('create', 'delete'),
'reference'               => &$GLOBALS['TL_LANG']['MSC'],
'eval'                    => array('multiple'=>true),
'sql'                     => "blob NULL"
);
