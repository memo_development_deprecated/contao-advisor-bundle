<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Miscellaneous
 */

$GLOBALS['TL_LANG']['MSC']['advisorPicker'] 							= 'Kundenberater';

/**
 * Content elements
 */
$GLOBALS['TL_LANG']['CTE']['advisor_listing_custom']					= array('Kundenberater Individuell', 'Kundenberater Einträge können hier auch ausserhalb der Archive mit eigener Sortierung ausgegeben werden.');
$GLOBALS['TL_LANG']['CTE']['advisor_listing_archive']					= array('Kundenberater nach Archiv', 'Ausgabe der Kundenberater Einträge nach Archiv');

$GLOBALS["TL_LANG"]["MSC"]["submit"] 									= "Absenden";
$GLOBALS["TL_LANG"]["MSC"]["search"]									= "Suche";
$GLOBALS["TL_LANG"]["MSC"]["location"]									= "Ort";
