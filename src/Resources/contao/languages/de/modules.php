<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['advisor']										= array('Kundenberater', 'Kundenberater verwalten');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['advisor_listing']								= array('Kundenberater Listenansicht');
$GLOBALS['TL_LANG']['FMD']['advisor_reader']								= array('Kundenberater Detailansicht');
$GLOBALS['TL_LANG']['FMD']['mod_advisor_module']							= array('Kundenberater');
