<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */
/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_memo_advisor']['general_legend']						= 'Generelle Informationen';
$GLOBALS['TL_LANG']['tl_memo_advisor']['publish_legend']						= 'Veröffentlichung';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_memo_advisor']['location']								= array('Ort', 'Ort des Eintrags');
$GLOBALS['TL_LANG']['tl_memo_advisor']['alias']									= array('Alias','Wie soll der Alias des Eintrags lauten? Der Alias muss eindeutig sein und wird für die URL verwendet.');
$GLOBALS['TL_LANG']['tl_memo_advisor']['canton']								= array('Kanton (Abkürzung)', 'Abkürzung des entsprechenden Kantons');
$GLOBALS['TL_LANG']['tl_memo_advisor']['zip']									= array('PLZ', 'PLZ des Eintrags');
$GLOBALS['TL_LANG']['tl_memo_advisor']['contact']								= array('Kundenberater', 'Kundenberater des Eintrags');
$GLOBALS['TL_LANG']['tl_memo_advisor']['keywords']								= array('Suchwörter', 'Kommaseparierte Liste, der Suchwörter (wird generiert)');

$GLOBALS['TL_LANG']['tl_memo_advisor']['published']								= array('Veröffentlicht','Soll dieser Eintrag auf der Webseite veröffentlicht werden?');
$GLOBALS['TL_LANG']['tl_memo_advisor']['start']									= array('Anzeigen ab','Wenn Sie den Eintrag erst ab einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');
$GLOBALS['TL_LANG']['tl_memo_advisor']['stop']									= array('Anzeigen bis','Wenn Sie den Eintrag nur bis zu einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_memo_advisor']['new']									= array('Neuer Eintrag', 'Neuer Eintrag anlegen');
$GLOBALS['TL_LANG']['tl_memo_advisor']['show']									= array('Details', 'Infos zum Eintrag mit der ID %s');
$GLOBALS['TL_LANG']['tl_memo_advisor']['edit']									= array('Eintrag bearbeiten ', 'Eintrag bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_advisor']['cut']									= array('Eintrag Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_memo_advisor']['copy']									= array('Eintrag Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_memo_advisor']['delete']								= array('Eintrag Löschen ', 'ID %s löschen');
