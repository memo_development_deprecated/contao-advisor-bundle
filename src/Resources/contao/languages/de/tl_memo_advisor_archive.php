<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['general_legend']				= 'Generelle Informationen';
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['media_legend']					= 'Medien';
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['publish_legend']				= 'Veröffentlichung';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['title']							= array('Titel', 'Name des Kundenberater');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['alias']							= array('Alias','Wie soll der Alias des Eintrags lauten? Der Alias muss eindeutig sein und wird als URL Alias verwendet.');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['jumpTo']						= array('Detailseite','Auf welcher Seite wurde das Reader-Modul eingebunden?');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['jumpToListing']					= array('Listenseiten','Auf welcher Seite wurde das Listen-Modul eingebunden?');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['category_groups']				= array('Kategorie Filter', 'Grenzen Sie die verfügbaren Kategorie-Gruppen für Kindelemente dieses Archives ein. Kind-Elemente können nur Kategorien von den hier definierten Gruppen erhalten. Leer = Alle Kategorien sind verfügbar.');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['description']					= array('Beschreibung', 'Beschreibung des Eintrags. Wird auf der Detailseite ausgegeben.');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['teaser']						= array('Teaser', 'Kurze Beschreibung des Eintrags. Wird auf der Übersichtsseite ausgegeben.');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['singleSRC']						= array('Bild','Bild des Eintrags. Wird auf der Übersichtsseite ausgegeben.');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['published']						= array('Veröffentlicht','Soll dieser Eintrag auf der Webseite veröffentlicht werden.');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['start']							= array('Anzeigen ab','Wenn Sie das Advisor erst ab einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['stop']							= array('Anzeigen bis','Wenn Sie das Advisor nur bis zu einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');


/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['new']							= array('Neues Kundenberater-Archiv', 'Neues Kundenberater-Archiv anlegen');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['show']							= array('Details', 'Infos zum Kundenberater-Archiv mit der ID %s');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['edit']							= array('Kundenberater-Archiv bearbeiten ', 'Kundenberater-Archive bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['editContact']					= array('Berater bearbeiten ', 'Berater bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['editheader']					= array('Kundenberater-Archiv bearbeiten ', 'Kundenberater-Archiv bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['cut']							= array('Kundenberater-Archiv Verschieben', 'Kundenberater-Archiv mit der ID %s verschieben');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['copy']							= array('Kundenberater-Archiv Duplizieren ', 'Kundenberater-Archiv mit der ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_memo_advisor_archive']['delete']						= array('Kundenberater-Archiv Löschen ', 'Kundenberater-Archiv mit der ID %s löschen');
