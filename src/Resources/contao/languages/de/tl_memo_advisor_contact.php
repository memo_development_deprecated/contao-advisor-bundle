<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */
/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['general_legend']						= 'Generelle Informationen';
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['publish_legend']						= 'Veröffentlichung';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['firstname']								= array('Vorname', 'Vorname des Beraters');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['lastname']								= array('Nachname','Nachname des Beraters');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['alias']									= array('Alias', 'Wie soll der Alias des Eintrags lauten? Der Alias muss eindeutig sein und wird als URL Alias verwendet.');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['function']								= array('Funktion', 'Funktion des Beraters');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['location']								= array('Einsatzgebiet', 'Einsatzgebiet des Beraterss');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['tel']									= array('Telefonnummer', 'Telefonnummer des Beraters');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['mobile']								= array('Handynummer', 'Handynummer des Beraters');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['email']									= array('E-Mail', 'E-Mail-Adresse des Beraters');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['singleSRC']								= array('Bild', 'Bild des Beraters');

$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['published']								= array('Veröffentlicht','Soll dieser Eintrag auf der Webseite veröffentlicht werden?');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['start']									= array('Anzeigen ab','Wenn Sie den Eintrag erst ab einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['stop']									= array('Anzeigen bis','Wenn Sie den Eintrag nur bis zu einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['new']									= array('Neuer Berater', 'Neuer Berater anlegen');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['show']									= array('Details', 'Infos zum Berater mit der ID %s');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['edit']									= array('Berater bearbeiten ', 'Berater bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['cut']									= array('Berater Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_memo_advisor_contact']['copy']									= array('Berater Duplizieren ', 'ID %s duplizieren');
 
 ?>