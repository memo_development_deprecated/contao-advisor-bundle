<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Legend
 */
$GLOBALS['TL_LANG']['tl_user']['advisor_legend'] 						= 'Kundenberater-Rechte';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_user']['advisors']							= array('Erlaubte Archive', 'Hier können Sie den Zugriff auf ein oder mehrere Kundenberater-Archive erlauben.');
$GLOBALS['TL_LANG']['tl_user']['advisorp']							= array('Archivrechte', 'Hier können Sie die Archivrechte festlegen.');
