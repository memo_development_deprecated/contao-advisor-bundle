<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

$GLOBALS["TL_LANG"]["MSC"]["submit"] 									= "Send";
$GLOBALS["TL_LANG"]["MSC"]["search"]									= "Search";
$GLOBALS["TL_LANG"]["MSC"]["location"]									= "Location";
