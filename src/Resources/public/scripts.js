jQuery(function($) {

	let keyupTimeout;

	$(document).ready(function() {
		var $inputFields = $("input.findAdvisor");

		init();

		$inputFields.each(function() {
			var $strForm = $(this).closest(".ce_form").html();
			var $objForm = $(this).closest(".ce_form");

			$objForm.siblings(".reload-button").on("click", function() {
				$objForm.html($strForm);

				init();
			});
		});
	});

	function init() {

		var $inputFields = $("input.findAdvisor");

		$inputFields.keyup(function() {

			var $inputField = $(this);
			var keyword = $inputField.val();
			var redirectPage = $inputField.closest("form").attr("action");
			var url = redirectPage + '?search=' + keyword;

			clearTimeout(keyupTimeout);
			keyupTimeout = setTimeout(function() {
				loadSearchResult(url, $inputField);

			}, 200)
		});

		$inputFields.each(function() {
			$(this).closest("form").submit(function(event) {
				event.preventDefault();
			});
		});
	}

	function loadSearchResult(url, $inputField) {
		var xhr = new XMLHttpRequest();
		let $input = $inputField;
		let inputValue = $inputField[0].value.trim();

		xhr.onreadystatechange = function() {
			if (xhr.readyState === 4) {
				$response = $(xhr.response).find(".advisor_item_full");

				let arrItems = [];
				$response.each(function() {
					$(this).find("a").on("click", function(event) {
						event.preventDefault();

						loadAdvisor($(this));

					});

					arrItems.push(this);
				});

				if ($response.length) {
					let elResultContainer = $input[0].parentNode.querySelector('div.searchResult');

					if (!elResultContainer) {
						elResultContainer = document.createElement('div');
						elResultContainer.classList.add('searchResult');
						$input[0].parentNode.appendChild(elResultContainer);
					}

					elResultContainer.innerHTML = '';

					arrItems.sort(function(a, b) {
						let aLink = a.querySelector('a');
						let bLink = b.querySelector('a');

						return aLink.innerHTML.indexOf(inputValue) - bLink.innerHTML.indexOf(inputValue);
					});

					for (let i = 0; i < arrItems.length; i++) {
						let elItem = arrItems[i];
						let elLink = elItem.querySelector('a');

						let regInputValue = new RegExp(inputValue, 'i');
						let match = regInputValue.exec(elLink.innerHTML)

						if (match) {
							elLink.innerHTML = elLink.innerHTML.replace(
								match,
								'<span class="highlight">' + match + '</span>'
							);
						}

						elResultContainer.appendChild(elItem);
					}
				} else {
					if ($("html").attr("lang") == "fr") {
						var strNoSearchResult = "<div style='padding: .5em .8em .5em'>Aucun résultat</div>";
					} else if ($("html").attr("lang") == "en") {
						var strNoSearchResult = "<div style='padding: .5em .8em .5em'>No result</div>";
					} else {
						var strNoSearchResult = "<div style='padding: .5em .8em .5em'>Kein Resultat</div>";
					}

					$input.siblings("div.searchResult").html(strNoSearchResult);
				}
			}
		}

		xhr.open('GET', url, true);
		xhr.send('');
	}

	function loadAdvisor($element) {
		var xhr = new XMLHttpRequest();
		var url = $element.attr("href");

		xhr.onreadystatechange = function() {
			if (xhr.readyState === 4) {
				$response = $(xhr.response).find(".advisor_contact_listing");
				$element.closest(".ce_form").html($response);
			}
		}

		xhr.open('GET', url, true);
		xhr.send('');
	}

});
