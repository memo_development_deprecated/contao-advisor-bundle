<?php declare(strict_types=1);

/**
 * @package   Memo\MemoAdvisorBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\AdvisorBundle\Security;

final class AdvisorPermissions
{
	public const USER_CAN_EDIT_ARCHIVE = 'contao_user.advisors';
	public const USER_CAN_CREATE_ARCHIVES = 'contao_user.advisorp.create';
	public const USER_CAN_DELETE_ARCHIVES = 'contao_user.advisorp.delete';
}
